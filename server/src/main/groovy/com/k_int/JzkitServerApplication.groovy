package com.k_int

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext;

@SpringBootApplication
class JzkitServerApplication {

    static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run JzkitServerApplication, args

        System.out.println("Inspect beans provided by Spring Boot:");
        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }

    }
}
