package com.k_int

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct

@Service
class JzkitService {
  private static final Logger logger = LoggerFactory.getLogger(JzkitService.class);

  @PostConstruct
  public def init() {
    logger.debug("Log message at DEBUG level from JzkitService init");
    logger.info("Log message at INFO level from JzkitService init");
  }

  public def listen() {
    logger.debug("Log message at DEBUG level from JzkitService listen");
  }
}
