/**
 * Title:       DummySearchable
 * @version:    $Id: DummySearchable.java,v 1.7 2004/11/18 13:44:37 ibbo Exp $
 * Copyright:   Copyright (C) 1999,2000 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ibbo@k-int.com)
 * Company:     Knowledge Integration Ltd.
 * Description: 
 *              
 */

package org.jzkit.search.provider.Dummy;

import java.util.Properties;
import java.util.*;

import org.jzkit.search.util.ResultSet.*;
import org.jzkit.search.provider.iface.*;
import org.springframework.context.*;


/**
 * @author Ian Ibbotson
 * @version $Id: DummySearchable.java,v 1.7 2004/11/18 13:44:37 ibbo Exp $
 */ 
public class DummySearchable implements Searchable
{
  private Map record_archetypes = new HashMap();
  private ApplicationContext ctx;

  public DummySearchable() {
  }

  public DummySearchable(int random_delay, String behaviour) {
  }

  public void setRandomDelay(int random_delay) {
  }

  public int getRandomDelay() {
    return 0;
  }

  public void setBehaviour(String behaviour) {
  }

  public String getBehaviour() {
    return null;
  }

  public void close() {
  }

  public IRResultSet evaluate(IRQuery q) {
    return evaluate(q,null,null);
  }

  public IRResultSet evaluate(IRQuery q, Object user_info) {
    return evaluate(q, user_info, null);
  }

  public IRResultSet evaluate(IRQuery q, Object user_info, Observer[] observers) {
    DummyResultSet result = new DummyResultSet();
    result.setStatus(IRResultSetStatus.COMPLETE);
    return result;
  }

  public void setRecordArchetypes(Map record_syntax_archetypes) {
    this.record_archetypes = record_archetypes;
  }
                                                                                                                                          
  public Map getRecordArchetypes() {
    return record_archetypes;
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }
}
