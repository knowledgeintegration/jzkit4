/**
 * Title:       DummyServiceDescription
 * @version:    $Id: DummySearchableFactory.java,v 1.5 2004/11/27 09:18:57 ibbo Exp $
 * Copyright:   Copyright (C) 1999,2000 Knowledge Integration Ltd.
 * @author:     Ian Ibbotson (ibbo@k-int.com)
 * Company:     Knowledge Integration Ltd.
 * Description: 
 *              
 */

package org.jzkit.search.provider.Dummy;

import java.util.Properties;
import java.util.Observer;

import org.jzkit.search.provider.iface.*;
import org.springframework.context.*;

/**
 * @author Ian Ibbotson
 */ 
public class DummySearchableFactory implements SearchServiceFactory {
                                                                                                                                          
  private static String[] prop_names = new String[] { "randomDelay" };
  private int random_delay = 10;
  private String behaviour = "normal";
  private ApplicationContext ctx;

  public Searchable newSearchable(ServiceUserInformation user_info) {
    Searchable s = new DummySearchable(random_delay, behaviour);
    s.setApplicationContext(ctx);
    return s;
  }

  public Searchable newSearchable() {
    Searchable s = new DummySearchable();
    s.setApplicationContext(ctx);
    return s;
  }

  public int getRandomDelay() {
    return random_delay;
  }

  public void setRandomDelay(int random_delay) {
    this.random_delay = random_delay;
  }

  public String getBehaviour() {
    return behaviour;
  }

  public void setBehaviour(String behaviour) {
    this.behaviour = behaviour;
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

  public String[] getPropertyNames() {
    return prop_names;
  }
}
